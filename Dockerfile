# from is used to fetch the base docker image
from alpine:latest

# RUN is executed when creating the image
# apk is the package manager for alpine linux
# --no-cache flag means do not use any local cache path
# python3-dev contains python3 and pip3
RUN apk add --no-cache python3-dev && pip3 install --upgrade pip

# WORKDIR command is similar to cd command
WORKDIR /app

# # Copy everything from the directory the Dockerfile is in to /app inside the image
COPY . /app

# # Install the required python packages
RUN pip3 --no-cache-dir install -r requirements.txt

# Port 5000 here is what I have specified in app.py, so we need to expose this port of the container when the app runs locally inside
EXPOSE 5000

# ENTRYPOINT command makes the container executable, which means the container will run the ENTRYPOINT command whenever it is created out of the image.
ENTRYPOINT ["python3"]

# CMD command is used to pass the arguments to the ENTRYPOINT, which makes python3 app.py
CMD ["app.py"]
